**SARONG**

A [sarong](https://www.allsarongs.com/All-Products-s/97.htm) (also called a pareo) is a piece of fabric, generally about 180 cm by 120 cm. A sarong is the traditional clothing of Indonesia and other parts of the world, for both men and women. 

Most sarongs are produced by the batik process. The word "batik" is the name for a process of resist dyeing and, by extension, for the type of bold exotic patterns used on fabrics dyed by this process. The process involves covering the areas of cloth which are not to be dyed with melted wax; the wax is later removed by immersion in boiling water. Using this slow and intricate process, executed by crafts people in small workshops, a single piece of batik may take more than a year to complete. Silk, rayon and cotton are used for batik. 

The batik process has been around for at least 2000 years. No one knows exactly when people first started applying wax, rice paste or even mud to cloth to resist dye. Batik is known to have existed in China, Japan, India, Indonesia, East Turkestan, Thailand, Europe and Africa. 

It was on the island of Java in the Indonesian archipelago where batik developed as one of the colossal works of art of Asia. Initially it was the pastime of privileged women and gradually batiked cloth became synonymous with aristocracy. As its popularity increased, more and more people became involved in batik production. Eventually, it became the national costume worn all over the islands. When the Dutch colonized Java in the seventeenth century, batik was introduced into Europe. 

**The use of a Sarong is limited only by your imagination. Here are some of the uses that we have come up with.** 

* Skirt. 
* Dress. 
* Jacket. 
* Wallhanging. 
* Tablecloth. 
* Automobile seat cover. 
* Gift for someone whose size you do not know. 
* Reusable gift wrapping. 
* Beach blanket. 
* Beach coverup. 
* Shawl. 
* Sash. 
* Bandana for an extremely large dog. 


